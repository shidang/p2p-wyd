/* 去除所有空格 */
function Trim(str, is_global) {
	var result;
	result = str.replace(/(^\s+)|(\s+$)/g, "");
	if (is_global.toLowerCase() == "g") {
		result = result.replace(/\s/g, "");
	}
	return result;
}/********************************end function Trim()*******************************************/

function mylogin() {
	
	var usrName = $("#myLoginName").val();
	
	if (null == Trim( usrName,'g') || ''==Trim( usrName,'g') ) {
		$("#myerrorshow").html('请输入登录用户名');
		$("#myerrorshow").css({
			color : "red"
		});
		return false;
	}
	
	var pwd = $("#myLoginPwd").val();
	
	if (null == Trim( pwd,'g') || ''==Trim( pwd,'g') ) {
		$("#myerrorshow").html('请输入登录密码');
		$("#myerrorshow").css({
			color : "red"
		});
		return false;
	}
	var loginUrl = $("#myurl").val();
	var rememberUser = $("#myRemember_user").attr("checked");
	
	$.ajax({
		type : 'post',
		url : '/registration/Itplogin',
		dataType : 'json',
		data : { userName : usrName,pwd : pwd, url : loginUrl,remember_user : rememberUser=='checked'?'true':'false'},
		success : function(call) {
			if (1 == call.status) {
				
				if (null != call.url && '' != call.url) {
					window.location.href = call.url;
				}else if ((null == call.url || '' == call.url)
						&& (null != call.callurl && '' != call.callurl)) {
					window.location.href = call.callurl;
				}
				
			} else {
				$("#myerrorshow").html(call.msg);
				$("#myerrorshow").css({
					color : "red"
				});
			}
		}
	});/*****end ajax *****************/
	
}

function ctlent(evt){
	var src = evt || evt.event; 
	if(src.keyCode==13){
		mylogin();
		return false;
	}else{return true;}
}
/************************************end function mylogin()*******************************************************/