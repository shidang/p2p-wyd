var $phoneReg = /^1[3,4,5,7,8]{1}[0-9]{1}[0-9]{8}$/;
var img_gou = '<img src="images/gougou.png"  style="border-top-width: 17px; margin-top: 13px; margin-left: 13px;">';
/* 去除所有空格 */
function Trim(str,is_global){ 
	var result; 
	result = str.replace(/(^\s+)|(\s+$)/g,""); 
	if(is_global.toLowerCase()=="g"){ 
		result = result.replace(/\s/g,""); 
	} 
	return result; 
}

/************************************jQuery Bind event begin ********************************************/
$(function(){
	//注册三步的变化
	$(".registstbtn").click(function(){
		reg();
	});
	$(".reginststtthree").click(function(){
		$(".registtopbg").css("background", "url(../images/registstep3.jpg) no-repeat");
		$(".reginststeptwo").css("display","none");
		$(".reginststepthree").css("display","block");
	});
	
	
	//信息输入框
	$("#registshuyong").blur(function(){
		if($("#registshuyong").val()==""){
			$("#registshuyong").val("请输入用户名");
		}
	});
	$("#registshuyong").focus(function(){
		if($("#registshuyong").val()=="请输入用户名"){
			$("#registshuyong").val("");
		}
	});
	$("#registshushou").blur(function(){
		if($("#registshushou").val()==""){
			$("#registshushou").val("请输入手机号码");
		}
	});
	$("#registshushou").focus(function(){
		if($("#registshushou").val()=="请输入手机号码"){
			$("#registshushou").val("");
		}
	});
	$("#registshuyan").blur(function(){
		if($("#registshuyan").val()==""){
			$("#registshuyan").val("请输入验证码");
		}
	});
	$("#registshuyan").focus(function(){
		if($("#registshuyan").val()=="请输入验证码"){
			$("#registshuyan").val("");
		}
	});
	$("#registshumi").blur(function(){
		if($("#registshumi").val()==""){
			$("#registshumi").val("请设置密码");
		}
	});
	$("#registshumi").focus(function(){
		if($("#registshumi").val()=="请设置密码"){
			$("#registshumi").val("");
		}
	});
	$("#registshuquemi").blur(function(){
		if($("#registshuquemi").val()==""){
			$("#registshuquemi").val("请确认输入密码");
		}
	});
	$("#registshuquemi").focus(function(){
		if($("#registshuquemi").val()=="请确认输入密码"){
			$("#registshuquemi").val("");
		}
	});
	$("#registshutui").blur(function(){
		if($("#registshutui").val()==""){
			$("#registshutui").val("没有可不填");
		}
	});
	$("#registshutui").focus(function(){
		if($("#registshutui").val()=="没有可不填"){
			$("#registshutui").val("");
		}
	});
});
/************************************jQuery Bind event end ********************************************/


function checkName2(){
	var vfId = document.getElementById("userName");
	var vfvl =$(vfId).val() ;//vfId.val Trim($(vfId).val(),'g');     //清除全部空格
	var arr=new Array();
	arr=vfvl.split(" ");
	if (null == vfvl || '' == vfvl) {
		$("#registerror").html('<span class="registerror"></span>注册用户名不能为空');
		$("#registerror").css({ color: "red"});
		return false;
	}else if(arr.length != 1){
		$("#registerror").html('<span class="registerror"></span>用户名不能有空格');
		$("#registerror").css({ color: "red"});
		return false;
	}else if(vfvl.length<6 || vfvl.length>20 ){
		$("#registerror").html('<span class="registerror"></span>字母、数字及下划线并以字母或数字开头,长度6-20位');
		$("#registerror").css({ color: "red"});
		return false;
	}
	
	if(!(/^([a-zA-Z]{1}|[0-9])[0-9a-zA-Z_]{5,19}$/.test(vfvl))){
		$("#registerror").html('<span class="registerror"></span>用户名以字母或数字开头,长度6-20位');
		$("#registerror").css({ color: "red"});
		return false;
	}
	return true;
}

function checkName() {
	
	if(checkName2() == false) return false;
	
	var fruit=checkUserName();
	if(!fruit){
		return false;
	}
	//$("#registerror").hide();
	$("#registerror").html(img_gou);
	$("#registerror").css({color:''});
	//$("#userNameGreen").css({display:'block'});
	return true;
	
} /*************** end function checkName *****************/


//验证用戶名是否存在
function checkUserName(){
	var fruit=true;
	$.ajax({
		async : false,
		type : 'post',
		url : '/registration/checkOnly_userName',
		data : {'userName' : Trim($("#userName").val(),'g')},
		dataType : 'text',
		success : function(msg) {
			if(msg=='0'){
				$("#registerror").html('<span class="registerror"></span>用户名已存在,请更换用户名');
				$("#registerror").css({ color: "red"});
				fruit=false;
			}			
		}
	});
	return fruit;
}/*********************end function checkUserName()***************************/

function checkPhone(){
	
	var phone = $("#phone").val();
	
	if(phone ==''||!$phoneReg.test(phone)){
		$("#registerror3").html('<span class="registerror"></span>请输入有效的手机号码');
		$("#registerror3").css({ color: "red"});
		return false;
	}
	
	if( chkOnlyPhone() == false) return  false;
	
	$("#registerror3").html(img_gou);
	$("#registerror3").css({ color: ''});
	return true;
}/***********************end function checkPhone() ***************************************/

function chkOnlyPhone(){
	
	var fruit=true;
	
	$.ajax({
		async : false,
		type : 'post',
		url : '/registration/checkOnly_phone',
		data : {'phone' : Trim($("#phone").val(),'g')},
		dataType : 'text',
		success : function(msg) {
			if(msg=='1'){
				$("#registerror3").html('<span class="registerror"></span>手机已注册');
				$("#registerror3").css({ color: "red"});
				fruit=false;
			}			
		}
	});
	return fruit;
}

function getPhoneCode2(){
	if(checkPhone() == false) return;
	
	var $phone = $('#phone').val();
	$.ajax({ 
			type:'post', 
			url:'/auth/verifySms', 	
			data:{telphone :$phone},
			dataType : 'text',
			success:function(data){ 
				if (data == "1") {
					ymPrompt.succeedInfo('手机验证码已成功发送至您的手机，请注意查收', 400, 200,
							'成功', null);
					waitVarifyWithSms(false);
				} else if(data == "2"){
					$time1 = 0;
	    			ymPrompt.errorInfo('该手机号码已经被注册！', 400, 200, '失败', null);
	    		}
				else if(data == "3"){
					$time1 = 0;
	    			ymPrompt.errorInfo('请输入手机号码！', 400, 200, '失败', null);
	    		}else {
					ymPrompt.errorInfo('手机验证码发送失败！', 400, 200, '失败', null);
				}
			} 
		});
}/*****************************end function getPhoneCode()**********************************************/


function checkPwd() {
	var vfId = document.getElementById("pwd");
	var vfvl = $(vfId).val();           //密码不能全为空格     允许5个空格1个字符的存在
	if (null == Trim(vfvl,'g') || ''==Trim(vfvl,'g') || vfvl.length<6 || vfvl.length>20) {
		$("#registerror1").html('<span class="registerror"></span>密码长度不能低于6位或大于20位');
		$("#registerror1").css({ color: "red"});
		return false;
	}else{
		var testpassword =/^(\d+[a-zA-Z]\w*)|([a-zA-Z]+\d\w*)$/; //验证密码是字母+数字的组合
		if(!testpassword.test(vfvl)){
			$("#registerror1").html('<span class="registerror"></span>密码只能是字母+数字的组合');
			$("#registerror1").css({ color: "red"});
			return false;
		}
	}
	$("#registerror1").html(img_gou);
	$("#registerror1").css({ color: ""});
	
	return true;
	
}/**********************************end function checkPwd()************************************************/

function chkRepeatPwd(){
	var pwd = $("#pwd").val();
	var repeatpwd = $("#repeatpwd").val();
	
	if (null == Trim(repeatpwd,'g') || ''==Trim(repeatpwd,'g') || repeatpwd.length<6 || repeatpwd.length>20) {
		$("#registerror2").html('<span class="registerror"></span>密码长度不能低于6位或大于20位');
		$("#registerror2").css({ color: "red"});
		return false;
	}else if( pwd != repeatpwd ){
			$("#registerror2").html('<span class="registerror"></span>密码不一致');
			$("#registerror2").css({ color: "red"});
		return false;
	}
	$("#registerror2").html(img_gou);
	$("#registerror2").css({ color:''});
	return true;
}/**********************************end function chkRepeatPwd()***********************************************/

function checkRm(){
	var vfId = document.getElementById("recommend");
	var vfvl = $(vfId).val();
	/*if(null != Trim(vfvl,'g') && ''!= Trim(vfvl,'g') && (Trim(vfvl,'g').length<6 || Trim(vfvl,'g').length>20)){
		$("#registerror4").html('<span class="registerror"></span>推荐人字段长度不能低于6位或大于20位');
		$("#registerror4").css({ color: "red"});
		return false;
	}else*/ 
	//alert(vfvl);	
	if(null != Trim(vfvl,'g') && ''!= Trim(vfvl,'g'))
	{
		var fruit=checkRecommend();
		if(fruit == false)
		{
			return false;
		}
		else
		{
			$("#registerror4").html(img_gou);
			$("#registerror4").css({ color: ''});
			return true;
		}
	}
	if(null==vfvl||vfvl.equals(""))
	{
		$("#registerror4").html('');
		$("#registerror4").css({ color: ''});
		return true;
	}
	return true;
}
/*****************************end function checkRm()********************************************/


//驗證推薦人是否存在
function checkRecommend(){
	var fruit=true;
	$.ajax({
		async : false,
		type : 'post',
		url : '/registration/coRecommend',
		data : {'unOrcm' : Trim($("#recommend").val(),'g')},
		dataType : 'json',
		success : function(msg) {
			if(!msg){
				$("#registerror4").html('<span class="registerror"></span>不存在此推荐人');
				$("#registerror4").css({ color: "red"});
				fruit=false;
			}			
		}
	});
	return fruit;
}
/****************************************end function checkRecommend() ************************************************/


function regOne() {
	$.ajax({
			async:false,
				type : 'post',
				url : '/registration/regMethod',
				data : {
					'userName' : Trim($("#userName").val(),'g'),
					'pwd' : $.trim($("#pwd").val()),
					'recommend' : Trim($("#recommend").val(),'g'),
					"phoneCode" : Trim($("#phoneCode").val(),'g'),
					"phone" : Trim($("#phone").val(),'g')
				},
				dataType:"text",
				success : function(msg) {
					if ('1' == msg) {
						//ymPrompt.succeedInfo('注册成功,请选择客服', 400, 200,
						//		'成功', null);
						//chooseKF();	
						$(".registtopbg").css("background", "url(../images/registstep2.jpg) no-repeat");
						$(".reginstxinxilist").css("display","none");
						$(".registstrightbg").css("display","none");
						$(".reginststeptwo").css("display","block");
					} else if('0'==msg){
						ymPrompt.succeedInfo('注册失败,请稍后再试', 400, 200,
								'失敗', null);
					}else if('5'==msg){
						ymPrompt.succeedInfo('验证码输入错误,请核实填写', 400, 200,
								'失敗', null);
					}else if( '4' == msg ){
						ymPrompt.errorInfo("手机动态码错误！");
					}else if( '3' == msg ){
						ymPrompt.errorInfo("手机动态码有效时间2h,该手机动态码失效,请重新获取！");
					}
				}
			});
}/******************************************end function regOne()************************************************/


function chckContract(){

	if($("#item").attr("checked") != 'checked' ){
		$("#registerror6").html('<span class="registerror"></span>请阅读网银贷服务条款');
		$("#registerror6").css({ color: "red"});
		return false;
	}
	
	$("#registerror6").html('');
	$("#registerror6").css({ color: ""});
	return true;
}

//点击注册时事件
function reg(){
	
	if(checkName() == false || checkPhone()== false 
			|| checkPwd() == false || chkRepeatPwd()== false 
			 || chckContract() == false ) 
		return;


	//检查验证码
	var bool = false;
	jQuery.ajax({
		type:'post',
		url:'/auth/AuthTel2',
		async: false,
		data:{"tel":$("#phone").val(),"getTelCode":$("#phoneCode").val()},
		dataType:"text",
		success:function(data,status){
			switch(parseInt(data)) {
			case 3:
				ymPrompt.errorInfo("手机动态码有效时间2h,该手机动态码失效,请重新获取！");
				break;
			case 4:
				ymPrompt.errorInfo("手机动态码错误！");
				break;
			case 5:
				ymPrompt.errorInfo("请获取验证码！");
				break;
			case 1:
				 bool = true;
				 break;
			}
		},
		error:function(){
			ymPrompt.errorInfo("发生异常，请与管理 员联系！");
		}
	});
	
	if(bool == true ){
		regOne();
	}
}/*******************************************end function reg()*************************************************/


/************************************************code 倒计时验证码 begin**************************************************/
var waitSms=-1;
var fruit=true;
function waitVarifyWithSms(stop){	
	var timer;
	if(stop){
		waitSms=0;
	}
	if(fruit){
		fruit=false;
		var obj=document.getElementById("getPhoneCode");
		obj.setAttribute("onclick", "javascript:return false;");			
	}
	if (-1 == waitSms) {
		waitSms = 60;
	}
	$("#getPhoneCode").val('请等待' + waitSms + 's');
	if (0 == waitSms) {
		window.clearTimeout(this.timer);
		fruit=true;
		var obj=document.getElementById("getPhoneCode");
		obj.setAttribute("onclick", "javascript:getPhoneCode2()");
		$("#getPhoneCode").val('点击获取');
	}					
	waitSms=waitSms-1;
	if(waitSms>-1){
		this.timer = window.setTimeout("waitVarifyWithSms(false)",1000);
	}				
}
/******************************************** code 倒讲时验证码 end**************************************************************/