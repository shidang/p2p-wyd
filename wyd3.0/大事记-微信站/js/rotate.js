/**
 * Created by voledy on 2015/7/22.
 */
window.onload=function(){
    var home=document.getElementById("home");
    var oStop=true;
    var iR=-150;
    var list=document.getElementById("main-list").getElementsByTagName("li");
    home.onclick=function(){
        if(oStop){
            home.style.webkitTransform="rotate(-360deg)";
            for(var i=0; i<list.length; i++){
                list[i].style.webkitTransition="0.5s "+i*100+"ms";
                list[i].style.left=getPos(iR,90/4*i).left+"px";
                list[i].style.top=getPos(iR,90/4*i).top+"px";
                list[i].style.webkitTransform="rotate(-720deg)";
            }
        }else{
            home.style.webkitTransform="rotate(0deg)";
            for(var i=0; i<list.length;i++){
                list[i].style.webkitTransition="0.5s "+(list.length-1-i)*100+"ms";
                list[i].style.left=0+"px";
                list[i].style.top=0+"px";
                list[i].style.webkitTransform="rotate(0deg)";
            }
        }
        oStop=!oStop;
    }
}
function getPos(iR,ideg){
    return{left:Math.round(Math.cos(ideg/180*Math.PI)*iR),top:Math.round(Math.sin(ideg/180*Math.PI)*iR)}
}
